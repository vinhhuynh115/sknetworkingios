Pod::Spec.new do |spec|
    spec.name         = 'SKNetworkingIOS'
    spec.version      = '0.0.3'
    spec.summary      = 'Summary'
    spec.homepage     = 'https://gitlab.com/ShanjiKun/SKNetworkingIOS'
    spec.license      = 'MIT'
    spec.author       = { 'ShanjiKun' => 'ShanjiKun@gmail.com' }
    spec.ios.deployment_target = '9.0'
    spec.source       = { :git => 'https://gitlab.com/ShanjiKun/sknetworkingios.git', :tag => spec.version }
    spec.swift_versions = ['5.0', '5.1']

    # Using source_files once you wanna include the all of sources code in pod
    spec.source_files  = 'SKNetworkingIOS/Sources/*.swift'

    # Using vendored_frameworks once you wanna hide the all of sources code in pod
    # spec.ios.vendored_frameworks = 'SKNetworkingIOS.framework'

    spec.dependency 'Alamofire'
    spec.dependency 'ObjectMapper'
    # specify version
    # spec.dependency 'Alamofire, '~> 1.0'
end
