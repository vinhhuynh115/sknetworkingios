//
//  NetworkMonitor.swift
//  FPTPlayNetworking
//
//  Created by Vinh Huynh on 2/21/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import Alamofire

public enum ConnectionType {
    case cellular
    case ethernetOrWiFi
    
    init(_ type: NetworkReachabilityManager.ConnectionType) {
        switch type {
        case .wwan:
            self = .cellular
        case .ethernetOrWiFi:
            self = .ethernetOrWiFi
        }
    }
}

public enum NetworkStatus {
    case unknown
    case reachable(ConnectionType)
    case notReachable
    
    init(_ status: NetworkReachabilityManager.NetworkReachabilityStatus) {
        switch status {
        case .notReachable:
            self = .notReachable
        case .reachable(let connectionType):
            self = .reachable(ConnectionType(connectionType))
        case .unknown:
            self = .unknown
        }
    }
}

public protocol NetworkMonitor {
    typealias NetworkStatusUpdateHandler = (NetworkStatus) -> Void
    func add(observer: AnyObject, handler: @escaping NetworkStatusUpdateHandler)
    func remove(observer: AnyObject)
}

public final class DefaultNetworkMonitor: NetworkMonitor {
    
    private struct Observer {
        weak var observer: AnyObject?
        let handler: NetworkStatusUpdateHandler
    }
    
    static let shared = DefaultNetworkMonitor()
    
    private lazy var observers: [Observer] = []
    private lazy var networkReachablilityManager: NetworkReachabilityManager? = NetworkReachabilityManager(host: "www.apple.com")
    
    public init() { monitor() }
    
    private func monitor() {
    }
    
    public func add(observer: AnyObject, handler: @escaping NetworkStatusUpdateHandler) {
        observers.append(Observer(observer: observer, handler: handler))
    }
    
    public func remove(observer: AnyObject) {
        observers = observers.filter({ $0.observer !== observer })
    }
    
    private func notifyObservers(_ status: NetworkStatus) {
        for observer in observers {
            DispatchQueue.main.async { observer.handler(status) }
        }
    }
}
