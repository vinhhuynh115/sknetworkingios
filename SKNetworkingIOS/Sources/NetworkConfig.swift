//
//  NetworkConfig.swift
//  FPTPlayNetworking
//
//  Created by Vinh Huynh on 2/18/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import Foundation

public protocol NetworkConfigurable {
    var baseURL: URL { get }
    var headers: [String: String] { get }
    var queries: [String: String] { get }
}

public struct NetworkConfig: NetworkConfigurable {
    public let baseURL: URL
    public let headers: [String : String]
    public let queries: [String : String]
    
    public init(baseURL: URL,
                headers: [String: String] = [:],
                queries: [String: String] = [:]) {
        
        self.baseURL = baseURL
        self.headers = headers
        self.queries = queries
    }
}
