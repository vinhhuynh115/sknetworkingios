//
//  NetworkService.swift
//  FPTPlayNetworking
//
//  Created by Vinh Huynh on 2/18/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import Alamofire

public enum NetworkServiceError: Error {
    case cancelled
    case notConnected
    case urlGeneration
    case noResponseData
    case generic(Error)
    case error(statusCode: Int, data: Data?)
}

// MARK: Network Cancelable
public protocol NetworkCancelable {
    func cancel()
}

extension URLSessionTask: NetworkCancelable { }

// MARK: Network Session Manager
public protocol NetworkSessionManager {
    typealias Completion = (Data?, URLResponse?, Error?) -> Void
    func request(with request: URLRequest, completion: @escaping Completion) -> NetworkCancelable?
}

public final class DefaultNetworkSessionManager: NetworkSessionManager {
    public init() {}
    public func request(with request: URLRequest, completion: @escaping Completion) -> NetworkCancelable? {
        let task = URLSession.shared.dataTask(with: request, completionHandler: completion)
        task.resume()
        return task
    }
}

public final class AlamofireSessionManager: NetworkSessionManager {
    static let shared = AlamofireSessionManager()
    private let sessionManager: Alamofire.SessionManager
    private(set) lazy var validStatusCode = [200, 202, 304, 400, 401, 405, 406, 410, 426, 429]
    
    private class func setupCache() {
        let diskCapacity = 1024*1024*1024 /// 1GB
        let memoryCapacity = 1024*1024*1024 /// 1GB
        let cache = URLCache(memoryCapacity: memoryCapacity, diskCapacity: diskCapacity, diskPath: "shared_cache")
        URLCache.shared = cache
    }
    
    private static var defaultConfiguration: URLSessionConfiguration {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        configuration.httpMaximumConnectionsPerHost = 1
        configuration.timeoutIntervalForRequest = 10
        configuration.timeoutIntervalForRequest = 10
        configuration.requestCachePolicy = .useProtocolCachePolicy
        configuration.urlCache = URLCache.shared
        return configuration
    }
    
    public init() {
        AlamofireSessionManager.setupCache()
        self.sessionManager = Alamofire.SessionManager(configuration: AlamofireSessionManager.defaultConfiguration)
    }
    
    public func request(with request: URLRequest, completion: @escaping Completion) -> NetworkCancelable? {
        return sessionManager.request(request)
            .validate(statusCode: validStatusCode)
            .responseData { dataResponse in
                completion(dataResponse.data, dataResponse.response, dataResponse.error)
        }.task
    }
}

// MARK: Network Service
public protocol NetworkService {
    typealias Completion = (Swift.Result<Data, NetworkServiceError>) -> Void
    func request(endpoint: Requestable, completion: @escaping Completion) -> NetworkCancelable?
}

public final class DefaultNetworkService {
    private let config: NetworkConfigurable
    private let logger: NetworkServiceLoggable
    private let networkSession: NetworkSessionManager
    
    public init(config: NetworkConfigurable,
                logger: NetworkServiceLoggable = NetworkServiceLogger(),
                networkSession: NetworkSessionManager = AlamofireSessionManager()) {
        self.config = config
        self.logger = logger
        self.networkSession = networkSession
    }
    
    private func request(with request: URLRequest, completion: @escaping Completion) -> NetworkCancelable? {
        self.logger.log(request)
        return self.networkSession.request(with: request) { [weak self] data, response, requestError in
            guard let self = self else { return }
            
            if let requestError = requestError {
                let error: NetworkServiceError
                if let response = response as? HTTPURLResponse {
                    error = .error(statusCode: response.statusCode, data: data)
                } else {
                    error = self.resolve(requestError)
                }
                self.logger.log(error)
                completion(.failure(error))
                
            } else {
                if let data = data {
                    self.logger.log(data, response: response)
                    completion(.success(data))
                } else {
                    let error = NetworkServiceError.noResponseData
                    self.logger.log(error)
                    completion(.failure(error))
                }
            }
        }
    }
    
    private func resolve(_ error: Error) -> NetworkServiceError {
        let code = URLError.Code(rawValue: (error as NSError).code)
        switch code {
        case .notConnectedToInternet: return .notConnected
        case .cancelled: return .cancelled
        default: return .generic(error)
        }
    }
}

extension DefaultNetworkService: NetworkService {
    @discardableResult
    public func request(endpoint: Requestable, completion: @escaping Completion) -> NetworkCancelable? {
        do {
            let urlRequest = try endpoint.makeRequest(with: self.config)
            return self.request(with: urlRequest, completion: completion)
        } catch {
            completion(.failure(.urlGeneration))
            return nil
        }
    }
}
