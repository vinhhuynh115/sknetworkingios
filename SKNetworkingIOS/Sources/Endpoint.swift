//
//  Endpoint.swift
//  FPTPlayNetworking
//
//  Created by Vinh Huynh on 2/18/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

public enum HTTPMethodType: String {
    case get     = "GET"
    case put     = "PUT"
    case post    = "POST"
    case head    = "HEAD"
    case delete  = "DELETE"
}

public enum BodyEncoding {
    case stringEncodingAscii
    case jsonSerializationData
}

enum RequestGenerationError: Error {
    case components
}

public protocol Requestable {
    var path: String { get }
    var isFullPath: Bool { get }
    var method: HTTPMethodType { get }
    var bodyEncoding: BodyEncoding { get }
    var bodyParameters: [String: Any] { get }
    var queryParameters: [String: Any] { get }
    var headerParameters: [String: String] { get }
}

public protocol ResponseRequestable: Requestable {
    var keyPath: String { get }
}

public final class Endpoint: ResponseRequestable {
    public let path: String
    public let keyPath: String
    public let isFullPath: Bool
    public let method: HTTPMethodType
    public let bodyEncoding: BodyEncoding
    public let bodyParameters: [String : Any]
    public let queryParameters: [String : Any]
    public let headerParameters: [String : String]
    
    public init(path: String,
                keyPath: String = "",
                isFullPath: Bool = false,
                method: HTTPMethodType = .get,
                bodyParameters: [String: Any] = [:],
                queryParameters: [String: Any] = [:],
                headerParameters: [String: String] = [:],
                bodyEncoding: BodyEncoding = .jsonSerializationData) {
        
        self.path = path
        self.method = method
        self.keyPath = keyPath
        self.isFullPath = isFullPath
        self.bodyEncoding = bodyEncoding
        self.bodyParameters = bodyParameters
        self.queryParameters = queryParameters
        self.headerParameters = headerParameters
    }
}

extension Requestable {
    func makeURL(with config: NetworkConfigurable) throws -> URL {
        let enpoint = isFullPath ? path : config.baseURL.absoluteString.appending(path)
        
        guard var urlComponents = URLComponents(string: enpoint) else { throw RequestGenerationError.components }
        let allQueries = queryParameters.merge(config.queries)
        let urlQueryItems = allQueries.map {
            URLQueryItem(name: $0.key, value: "\($0.value)")
        }
        
        urlComponents.queryItems = urlQueryItems.isEmpty ? nil : urlQueryItems
        guard let url = urlComponents.url else { throw RequestGenerationError.components }
        return url
    }
    
    func makeRequest(with config: NetworkConfigurable) throws -> URLRequest {
        let url = try self.makeURL(with: config)
        let allHeaders = config.headers.merge(headerParameters)
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        urlRequest.allHTTPHeaderFields = allHeaders
        if !bodyParameters.isEmpty {
            urlRequest.httpBody = encodeBody(parameters: bodyParameters, encoding: bodyEncoding)
        }
        return urlRequest
    }
    
    fileprivate func encodeBody(parameters: [String: Any], encoding: BodyEncoding) -> Data? {
        switch encoding {
        case .jsonSerializationData:
            return try? JSONSerialization.data(withJSONObject: parameters)
        case .stringEncodingAscii:
            return parameters.queryString.data(using: .ascii, allowLossyConversion: true)
        }
    }
}

fileprivate extension Dictionary {
    var queryString: String {
        return map { "\($0.key)=\($0.value)" }
            .joined(separator: "&")
            .addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
    }
    
    func merge(_ otherDict: Dictionary) -> Dictionary {
        var newDict = self
        for (key, value) in otherDict {
            newDict.updateValue(value, forKey: key)
        }
        return newDict
    }
}
