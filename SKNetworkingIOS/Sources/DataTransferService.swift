//
//  DataTransferService.swift
//  FPTPlayNetworking
//
//  Created by Vinh Huynh on 2/18/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import Foundation
import ObjectMapper

public enum DataTransferError: Error {
    case parsing(String)
}

public protocol DataTransferService {
    typealias Completion<T> = (Result<T, Error>) -> Void
    func request<T: Decodable>(endpoint: ResponseRequestable, queue: DispatchQueue, completion: @escaping Completion<T>) -> NetworkCancelable?
    func request<T: BaseMappable>(endpoint: ResponseRequestable, queue: DispatchQueue, completion: @escaping Completion<T>) -> NetworkCancelable?
    func request<T: BaseMappable>(endpoint: ResponseRequestable, queue: DispatchQueue, completion: @escaping Completion<[T]>) -> NetworkCancelable?
}

public extension DataTransferService {
    @discardableResult
    func request<T: Decodable>(endpoint: ResponseRequestable,
                               queue: DispatchQueue = DispatchQueue.global(qos: .userInitiated),
                               completion: @escaping Completion<T>) -> NetworkCancelable? {
        return request(endpoint: endpoint, queue: queue, completion: completion)
    }
    
    @discardableResult
    func request<T: BaseMappable>(endpoint: ResponseRequestable,
                                       queue: DispatchQueue = DispatchQueue.global(qos: .userInitiated),
                                       completion: @escaping Completion<T>) -> NetworkCancelable? {
        return request(endpoint: endpoint, queue: queue, completion: completion)
    }
    
    @discardableResult
    func request<T: BaseMappable>(endpoint: ResponseRequestable,
                                       queue: DispatchQueue = DispatchQueue.global(qos: .userInitiated),
                                       completion: @escaping Completion<[T]>) -> NetworkCancelable? {
        return request(endpoint: endpoint, queue: queue, completion: completion)
    }
}

public class DefaultDataTransferService {
    private let logger: DataTransferLoggable
    private let networkService: NetworkService
    
    public init(with networkService: NetworkService,
                logger: DataTransferLoggable = DataTransferLogger()) {
        
        self.logger = logger
        self.networkService = networkService
    }
}

extension DefaultDataTransferService: DataTransferService {
    
    /// Returns a NetworkCancelable type, completes a Decodable type or an Error
    public func request<T: Decodable>(endpoint: ResponseRequestable,
                                      queue: DispatchQueue,
                                      completion: @escaping Completion<T>) -> NetworkCancelable? {
        
        return self.networkService.request(endpoint: endpoint) { [weak self] result in
            guard let self = self else { return }
            queue.async {
                switch result {
                case .success(let data):
                    let result: Result<T, Error> = self.decode(data)
                    DispatchQueue.main.async { completion(result) }
                case .failure(let error):
                    self.logger.log(error)
                    DispatchQueue.main.async { completion(.failure(error)) }
                }
            }
        }
    }
    
    /// Returns a NetworkCancelable type, completes an ImmutableMapple type or an Error
    public func request<T: BaseMappable>(endpoint: ResponseRequestable,
                                              queue: DispatchQueue,
                                              completion: @escaping Completion<T>) -> NetworkCancelable? {
        
        return self.networkService.request(endpoint: endpoint) { [weak self] result in
            guard let self = self else { return }
            queue.async {
                switch result {
                case .success(let data):
                    let result: Result<T, Error> = self.decode(data, at: endpoint.keyPath)
                    DispatchQueue.main.async { completion(result) }
                case .failure(let error):
                    self.logger.log(error)
                    DispatchQueue.main.async { completion(.failure(error)) }
                }
            }
        }
    }
    
    /// Returns a NetworkCancelable type, completes a `[ImmutableMapple]` type or an Error
    public func request<T: BaseMappable>(endpoint: ResponseRequestable,
                                              queue: DispatchQueue,
                                              completion: @escaping Completion<[T]>) -> NetworkCancelable? {
        
        return self.networkService.request(endpoint: endpoint) { [weak self] result in
            guard let self = self else { return }
            queue.async {
                switch result {
                case .success(let data):
                    let result: Result<[T], Error> = self.decode(data, at: endpoint.keyPath)
                    DispatchQueue.main.async { completion(result) }
                case .failure(let error):
                    self.logger.log(error)
                    DispatchQueue.main.async { completion(.failure(error)) }
                }
            }
        }
    }
}

// MARK: Decode
extension DefaultDataTransferService {
    /// Returns a Decodable type or an Error
    /// Supports the types: Data, String
    private func decode<T: Decodable>(_ data: Data) -> Result<T, Error> {
        if T.self is Data.Type, let data = data as? T {
            return .success(data)
        } else if T.self is String.Type, let string = String(decoding: data, as: UTF8.self) as? T {
            return .success(string)
        } else {
            let error = DataTransferError.parsing("Decode \(T.self) failed. This type doesn't yet support or parsing error.")
            logger.log(error)
            return .failure(error)
        }
    }
    
    /// Returns an ImmutableMapple type or an Error
    /// @parameter: keyPath is used to find a subJson out to map the object
    private func decode<T: BaseMappable>(_ data: Data, at keyPath: String) -> Result<T, Error> {
        do {
            let json: [String: Any] = try self.serialize(data, at: keyPath)
            guard let decodedObject = T(JSON: json) else {
                return .failure(DataTransferError.parsing("Decode \(T.self) failed."))
            }
            return .success(decodedObject)
        } catch {
            let error = DataTransferError.parsing("Decode \(T.self) failed. \(error).")
            logger.log(error)
            return .failure(error)
        }
    }
    
    /// Returns a `[ImmutableMapple]` type or an Error
    /// @parameter: keyPath is used to find a subJson out to map the array objects
    private func decode<T: BaseMappable>(_ data: Data, at keyPath: String) -> Result<[T], Error> {
        do {
            let json: [[String: Any]] = try self.serialize(data, at: keyPath)
            let decodedArrayObjects = [T](JSONArray: json)
            return .success(decodedArrayObjects)
        } catch {
            let error = DataTransferError.parsing("Decode \(T.self) failed. \(error).")
            logger.log(error)
            return .failure(error)
        }
    }
    
    /// Returns an type or throw an error
    /// Typically, the types are `[String: Any]` and `[[String: Any]]`
    private func serialize<T>(_ data: Data, at keyPath: String) throws -> T {
        let makeError: (Int) -> Error = { code in
            NSError(domain: "Decode \(T.self) failed. Serializing data error.", code: code, userInfo: nil)
        }
        
        var jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
        if keyPath.isEmpty {
            guard let castedJson = jsonObject as? T else { throw makeError(-1)}
            return castedJson
        } else {
            for path in keyPath.components(separatedBy: "/") {
                guard let dict = jsonObject as? [String: Any], let childJson = dict[path] else { throw makeError(-2)}
                jsonObject = childJson
            }
            guard let castedJson = jsonObject as? T else { throw makeError(-3)}
            return castedJson
        }
    }
}
