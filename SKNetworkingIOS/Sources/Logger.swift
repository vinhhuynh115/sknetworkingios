//
//  Logger.swift
//  FPTPlayNetworking
//
//  Created by Vinh Huynh on 2/20/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

// MARK: Network Service Loggable
public protocol DataTransferLoggable {
    func log(_ error: Error)
}

public final class DataTransferLogger: DataTransferLoggable {
    public init () { }
    
    public func log(_ error: Error) {
        print("---------------------------------")
        print("***** Data Transfer Service *****\n")
        print("\(error)")
        print("---------------------------------\n")
    }
}

// MARK: Network Service Loggable
public protocol NetworkServiceLoggable {
    func log(_ error: Error)
    func log(_ request: URLRequest)
    func log(_ data: Data?, response: URLResponse?)
}

public final class NetworkServiceLogger: NetworkServiceLoggable {
    public init() { }
    
    public func log(_ error: Error) {
        print("---------------------------")
        print("***** Network Service *****\n")
        print("\(error)")
        print("---------------------------\n")
    }
    
    public func log(_ request: URLRequest) {
        print("---------------------------")
        print("***** Network Service *****\n")
        print("REQUESTING...\n")
        print("Url: \(request.url?.absoluteString ?? "Empty")")
        print("Headers: \(request.allHTTPHeaderFields ?? [:])")
        print("Method: \(request.httpMethod ?? "Empty")")
        if let data = request.httpBody {
            data.log(prefix: "Body")
        } else {
            print("Body: Empty")
        }
        print("---------------------------\n")
    }
    
    public func log(_ data: Data?, response: URLResponse?) {
        print("---------------------------")
        print("***** Network Service *****\n")
        print("RESPONSING...\n")
        if let statusCode = (response as? HTTPURLResponse)?.statusCode {
            print("Code: \(statusCode)")
        } else {
            print("Code: Unknown")
        }
        if let data = data {
            data.log(prefix: "Data")
        } else {
            print("Data: Empty")
        }
        print("--------------------------\n")
    }
}

// MARK: Extension
fileprivate extension Data {
    func log(prefix: String) {
        if let json = try? JSONSerialization.jsonObject(with: self, options: []) as? [String: Any] {
            print("\(prefix): \(json)")
        } else if let json = try? JSONSerialization.jsonObject(with: self, options: []) as? [[String: Any]] {
            print("\(prefix): \(json)")
        } else if let string = String(data: self, encoding: .utf8) {
            print("\(prefix): \(string)")
        } else {
            print("\(prefix): Unknown")
        }
    }
}
