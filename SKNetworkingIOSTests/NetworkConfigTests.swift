//
//  NetworkConfigTests.swift
//  FPTPlayNetworkingTests
//
//  Created by Vinh Huynh on 2/18/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import XCTest
@testable import SKNetworkingIOS

class NetworkConfigTests: XCTestCase {
    
    func test_whenInitalWithBasedURL_shouldContainProperBaseURL() {
        /// Given
        let expectedURL = "https://www.apple.com"
        let expectedHeaders = ["header_key": "header_value"]
        let expectedQueries = ["query_key": "query_value"]
        
        let baseURL = URL(string: expectedURL)!
        let sut = NetworkConfig(baseURL: baseURL,
                                headers: expectedHeaders,
                                queries: expectedQueries)
        
        /// Then
        XCTAssertEqual(sut.baseURL.absoluteString, expectedURL, "Should contain the expected URL")
        XCTAssertEqual(sut.headers, expectedHeaders, "Should contain the expected headers")
        XCTAssertEqual(sut.queries, expectedQueries, "Should contain the expected queries")
    }
}

