//
//  EndpointMock.swift
//  FPTPlayNetworkingTests
//
//  Created by Vinh Huynh on 2/21/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

@testable import SKNetworkingIOS

struct EndpointMock: Requestable {
    var path: String
    var keyPath: String = ""
    var isFullPath: Bool = false
    var method: HTTPMethodType = .get
    var bodyParameters: [String: Any] = [:]
    var queryParameters: [String: Any] = [:]
    var headerParameters: [String: String] = [:]
    var bodyEncoding: BodyEncoding = .jsonSerializationData
    
    init(path: String,
         isFullPath: Bool = false) {
        
        self.path = path
        self.isFullPath = isFullPath
    }
}
