//
//  NetworkServiceMock.swift
//  FPTPlayNetworkingTests
//
//  Created by Vinh Huynh on 2/19/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

@testable import SKNetworkingIOS

struct NetworkServiceMock: NetworkService {
    
    let data: Data?
    let error: NetworkServiceError?
    
    init(data: Data? = nil,
         error: NetworkServiceError? = nil) {
        
        self.data = data
        self.error = error
    }
    
    func request(endpoint: Requestable, completion: @escaping Completion) -> NetworkCancelable? {
        if let error = error {
            completion(.failure(error))
        } else if let data = data {
            completion(.success(data))
        } else {
            completion(.failure(.noResponseData))
        }
        return URLSessionTask()
    }
}
