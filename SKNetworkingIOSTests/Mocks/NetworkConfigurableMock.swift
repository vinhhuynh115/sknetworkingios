//
//  NetworkConfigurableMock.swift
//  FPTPlayNetworkingTests
//
//  Created by Vinh Huynh on 2/18/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import Foundation
@testable import SKNetworkingIOS

class NetworkConfigurableMock: NetworkConfigurable {
    var baseURL: URL = URL(string: "http://mock.test.com")!
    var version: String = "api_v3"
    var secretKey: String = "123456"
    var headers: [String : String] = [:]
    var queries: [String : String] = [:]
    var baseVersionURL: String {
        return baseURL.absoluteString.appending("/").appending(version)
    }
}
