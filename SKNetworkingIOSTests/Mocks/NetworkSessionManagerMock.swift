//
//  NetworkSessionManagerMock.swift
//  FPTPlayNetworkingTests
//
//  Created by Vinh Huynh on 2/21/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

@testable import SKNetworkingIOS

struct NetworkSessionManagerMock: NetworkSessionManager {
    
    var data: Data?
    var error: Error?
    var response: URLResponse?
    
    init(data: Data? = nil,
         error: Error? = nil,
         response: URLResponse? = nil) {
        
        self.data = data
        self.error = error
        self.response = response
    }
    
    func request(with request: URLRequest, completion: @escaping Completion) -> NetworkCancelable? {
        completion(data, response, error)
        return URLSessionTask()
    }
}
