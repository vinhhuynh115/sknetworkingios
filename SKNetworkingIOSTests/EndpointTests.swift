//
//  EndpointTests.swift
//  FPTPlayNetworkingTests
//
//  Created by Vinh Huynh on 2/18/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import XCTest
@testable import SKNetworkingIOS

class EndpointTests: XCTestCase {
    
    func test_spellingHTTPMethodTypeRawValue() {
        XCTAssertEqual(HTTPMethodType.get.rawValue, "GET", "HTTP Method raw value incorrectly")
        XCTAssertEqual(HTTPMethodType.put.rawValue, "PUT", "HTTP Method raw value incorrectly")
        XCTAssertEqual(HTTPMethodType.post.rawValue, "POST", "HTTP Method raw value incorrectly")
        XCTAssertEqual(HTTPMethodType.head.rawValue, "HEAD", "HTTP Method raw value incorrectly")
        XCTAssertEqual(HTTPMethodType.delete.rawValue, "DELETE", "HTTP Method raw value incorrectly")
    }
    
    func test_whenInitEndpoint_shouldContainCorrectProperties() {
        /// Given
        let expectedPath = "/user"
        let expectedKeyPath = "data"
        let expectedIsFullPath = false
        let expectedMethod = HTTPMethodType.get
        let expectedBodyEncoding = BodyEncoding.jsonSerializationData
        let expectedBodyParameters: [String: Any] = ["body_key": "body_value"]
        let expectedQueryParameters: [String: Any] = ["query_key": "query_value"]
        let expectedHeaderParameters: [String: String] = ["header_key": "header_value"]
        
        let sut = Endpoint(path: expectedPath,
                           keyPath: expectedKeyPath,
                           isFullPath: expectedIsFullPath,
                           method: expectedMethod,
                           bodyParameters: expectedBodyParameters,
                           queryParameters: expectedQueryParameters,
                           headerParameters: expectedHeaderParameters,
                           bodyEncoding: expectedBodyEncoding)
        
        /// Then
        XCTAssertEqual(sut.path, expectedPath, "Should contain the expected path")
        XCTAssertEqual(sut.method, expectedMethod, "Should contain the expected method")
        XCTAssertEqual(sut.keyPath, expectedKeyPath, "Should contain the expected key path")
        XCTAssertEqual(sut.isFullPath, expectedIsFullPath, "Should contain the expected isFullPath")
        XCTAssertEqual(sut.bodyEncoding, expectedBodyEncoding, "Should contain the expected body enconding")
        XCTAssertEqual(sut.headerParameters, expectedHeaderParameters, "Should contain the expected header parameters")
        XCTAssertEqual(sut.bodyParameters.first?.value as? String, expectedBodyParameters.first?.value as? String, "Should contain the expected body parameter")
        XCTAssertEqual(sut.queryParameters.first?.value as? String, expectedQueryParameters.first?.value as? String, "Should contain the expected query parameter")
    }
    
    func test_whenMakeURLWithShortPath_shouldReturnProperURL() {
        /// Given
        let path = "/usertest"
        let endpointQueries = ["query_key1": "query_value1"]
        let configQueries = ["query_key2": "query_value2"]
        let networkConfigurableMock = NetworkConfigurableMock()
        networkConfigurableMock.queries = configQueries
        
        let expectedHost = networkConfigurableMock.baseURL.host
        let expectedRelativePath = URL(string: "\(networkConfigurableMock.baseURL.absoluteString)\(path)")!.relativePath
        let expectedQueries = Array(
            [
                endpointQueries.map { "\($0.key)=\($0.value)" },
                configQueries.map { "\($0.key)=\($0.value)" },
            ].joined()
        )
        
        let sut = Endpoint(path: path,
                           queryParameters: endpointQueries)
        
        /// When
        do {
            let url = try sut.makeURL(with: networkConfigurableMock)
            XCTAssertEqual(url.host, expectedHost, "Should return the expected host")
            XCTAssertEqual(url.relativePath, expectedRelativePath, "Should return the expected relative path")
            if let queries = url.query?.components(separatedBy: "&") {
                print(queries.joined(separator: "&"), expectedQueries.joined(separator: "&"))
                expectedQueries.forEach({
                    XCTAssertTrue(queries.contains($0), "Should return the expected queries")
                })
            } else {
                XCTFail("Should return the expected quries")
            }
        } catch {
            XCTFail("Should return \(error)")
        }
    }
    
    func test_whenMakeURLWithFullPath_shouldReturnProperURL() {
        /// Given
        let fullPath = "htttp://testfull.com/usertest"
        let endpointQueries = ["query_key1": "query_value1"]
        let configQueries = ["query_key2": "query_value2"]
        let networkConfigurableMock = NetworkConfigurableMock()
        networkConfigurableMock.queries = configQueries
        
        let expectedHost = URL(string: fullPath)!.host
        let expectedRelativePath = URL(string: fullPath)!.relativePath
        let expectedQueries = Array(
            [
                endpointQueries.map { "\($0.key)=\($0.value)" },
                configQueries.map { "\($0.key)=\($0.value)" },
                ].joined()
        )
        
        let sut = Endpoint(path: fullPath,
                           isFullPath: true,
                           queryParameters: endpointQueries)
        
        /// When
        do {
            let url = try sut.makeURL(with: networkConfigurableMock)
            XCTAssertEqual(url.host, expectedHost, "Should return the expected host")
            XCTAssertEqual(url.relativePath, expectedRelativePath, "Should return the expected relative path")
            if let queries = url.query?.components(separatedBy: "&") {
                print(queries.joined(separator: "&"), expectedQueries.joined(separator: "&"))
                expectedQueries.forEach({
                    XCTAssertTrue(queries.contains($0), "Should return the expected queries")
                })
            } else {
                XCTFail("Should return the expected quries")
            }
        } catch {
            XCTFail("Should return \(error)")
        }
    }
    
    func test_whenMakeRequest_shouldReturnProperRequest() {
        /// Given
        let path = "/usertest"
        let bodyParameters = ["body_key": "body_value"]
        let endpointQueries = ["query_key1": "query_value1"]
        let endpointHeaders = ["header_key1": "header_value1"]
        let configQueries = ["query_key2": "query_value2"]
        let configHeaders = ["header_key2": "header_value2"]
        let bodyEncoding = BodyEncoding.jsonSerializationData
        let networkConfigurableMock = NetworkConfigurableMock()
        networkConfigurableMock.queries = configQueries
        networkConfigurableMock.headers = configHeaders
        
        var expectedHeaders = configHeaders
        endpointHeaders.forEach({
            expectedHeaders.updateValue($0.value, forKey: $0.key)
        })
        
        let expectedMethod = HTTPMethodType.post
        
        let sut = Endpoint(path: path,
                           method: expectedMethod,
                           bodyParameters: bodyParameters,
                           queryParameters: endpointQueries,
                           headerParameters: endpointHeaders,
                           bodyEncoding: bodyEncoding)
        
        /// When
        do {
            let request = try sut.makeRequest(with: networkConfigurableMock)
            XCTAssertEqual(request.httpMethod, expectedMethod.rawValue, "Should return the expected method")
            let httpBodyDecoded = try? JSONSerialization.jsonObject(with: request.httpBody ?? Data(), options: .allowFragments) as? [String: String]
            bodyParameters.forEach({
                XCTAssertEqual(httpBodyDecoded?[$0.key], $0.value, "Should return the expected body data")
            })
            expectedHeaders.forEach({
                XCTAssertEqual(request.allHTTPHeaderFields?[$0.key], $0.value, "Should return the expected headers")
            })
        } catch {
            XCTFail("Should return \(error)")
        }
    }
    
    func test_whenMakeRequesetWithStringEncodingAsciiBodyEncoding_shouldReturnProperRequest() {
        /// Given
        let bodyParameters = ["body_key": "body_value"]
        let expectedBody = bodyParameters.map { "\($0.key)=\($0.value)" }
        let sut = Endpoint(path: "/usertest",
                           method: .post,
                           bodyParameters: bodyParameters,
                           bodyEncoding: .stringEncodingAscii)
        
        /// When
        do {
            let request = try sut.makeRequest(with: NetworkConfigurableMock())
            if let data = request.httpBody,
                let queryString = String(data: data, encoding: .utf8) {
                let queries = queryString.components(separatedBy: "&")
                print(queryString, expectedBody.joined(separator: "&"))
                expectedBody.forEach({
                    XCTAssertTrue(queries.contains($0), "Should return the expected body")
                })
            } else {
                XCTFail("Should return the expected body")
            }
        } catch {
            XCTFail("Should return \(error)")
        }
    }
}
