//
//  DataTransferServiceTests.swift
//  FPTPlayNetworkingTests
//
//  Created by Vinh Huynh on 2/19/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import XCTest
@testable import ObjectMapper
@testable import SKNetworkingIOS

class DataTransferServiceTests: XCTestCase {
    
    struct UserMock: ImmutableMappable {
        
        let age: Int
        var email: String
        let username: String
        
        init(map: Map) throws {
            age = try map.value("age")
            email = try map.value("email")
            username = try map.value("username")
        }
    }
    
    struct MutableMock: Mappable {
        var name: String = ""
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            self.name <- map["name"]
        }
    }
    
    func test_whenNetworkServiceRequestingFailed_shouldReturnNetworkServiceError() {
        /// Given
        let expectationMessage = "should return the network service error"
        let expectationDecodable = self.expectation(description: "Decodable request \(expectationMessage)")
        let expectationImmutationMapple = self.expectation(description: "ImmutationMapple request \(expectationMessage)")
        let expectationImmutationMappleArray = self.expectation(description: "ImmutationMapple Array request \(expectationMessage)")
        let sut = DefaultDataTransferService(with: NetworkServiceMock(error: .notConnected))
        /// When
        sut.request(endpoint: Endpoint(path: "")) { (result: Result<Data, Error>) in
            switch result {
            case .success:
                XCTFail("Should not happen")
            case .failure(let error):
                if error is NetworkServiceError {
                    expectationDecodable.fulfill()
                }
            }
        }
        sut.request(endpoint: Endpoint(path: "")) { (result: Result<UserMock, Error>) in
            switch result {
            case .success:
                XCTFail("Should not happen")
            case .failure(let error):
                if error is NetworkServiceError {
                    expectationImmutationMapple.fulfill()
                }
            }
        }
        sut.request(endpoint: Endpoint(path: "")) { (result: Result<[UserMock], Error>) in
            switch result {
            case .success:
                XCTFail("Should not happen")
            case .failure(let error):
                if error is NetworkServiceError {
                    expectationImmutationMappleArray.fulfill()
                }
            }
        }
        /// Then
        wait(for: [expectationDecodable, expectationImmutationMapple, expectationImmutationMappleArray], timeout: 0.1)
    }
    
    // MARK: Testing Request Generics Conform Decodable
    func test_whenRequestDecodedData_shouldReturnData() {
        /// Given
        let expectation = self.expectation(description: "Should return Data")
        let expectedData = "String Type".data(using: .utf8) ?? Data()
        let sut = DefaultDataTransferService(with: NetworkServiceMock(data: expectedData))
        /// When
        sut.request(endpoint: Endpoint(path: "")) { (result: Result<Data, Error>) in
            switch result {
            case .success(let data):
                if data == expectedData {
                    expectation.fulfill()
                } else {
                    XCTFail("Should return the expected data")
                }
            case .failure(let error):
                XCTFail("Should not return \(error.localizedDescription)")
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_whenRequestDecodedString_shouldReturnString() {
        /// Given
        let expectation = self.expectation(description: "Should return String")
        let expectedString = "String Type"
        let dataMock = expectedString.data(using: .utf8) ?? Data()
        let sut = DefaultDataTransferService(with: NetworkServiceMock(data: dataMock))
        /// When
        sut.request(endpoint: Endpoint(path: "")) { (result: Result<String, Error>) in
            switch result {
            case .success(let string):
                if string == expectedString {
                    expectation.fulfill()
                } else {
                    XCTFail("Should return the expected string")
                }
            case .failure(let error):
                XCTFail("Should not return \(error.localizedDescription)")
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_whenRequestFailedDecodeDecodable_shouldReturnParsingError() {
        /// Given
        let expectation = self.expectation(description: "Should return parsing error")
        let dataMock = try? JSONSerialization.data(withJSONObject: ["key": "value"], options: .prettyPrinted)
        let sut = DefaultDataTransferService(with: NetworkServiceMock(data: dataMock))
        /// When
        sut.request(endpoint: Endpoint(path: "")) { (result: Result<[String: String], Error>) in
            switch result {
            case .success:
                XCTFail("Should not happen")
            case .failure(let error):
                if case DataTransferError.parsing = error {
                    expectation.fulfill()
                } else {
                    XCTFail("Should not return error: \(error.localizedDescription)")
                }
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    // MARK: Testing Request Generics Conform BaseMappable
    func test_whenRequestFailedDecodeObject_shouldReturnParsingError() {
        /// Given
        let expectation = self.expectation(description: "Should return parsing error")
        let sut = DefaultDataTransferService(with: NetworkServiceMock(data: Data()))
        /// When
        sut.request(endpoint: Endpoint(path: "")) { (result: Result<UserMock, Error>) in
            switch result {
            case .success:
                XCTFail("Should not happen")
            case .failure(let error):
                if case DataTransferError.parsing = error {
                    expectation.fulfill()
                } else {
                    XCTFail("Should not return error: \(error.localizedDescription)")
                }
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_whenRequestDecodedObject_shouldReturnProperObject() {
        /// Given
        let expectation = self.expectation(description: "Should return proper object")
        let jsonMock = ["age": 18, "email": "test@gmail.com", "username": "test"] as Any
        let dataMock = try? JSONSerialization.data(withJSONObject: jsonMock, options: .prettyPrinted)
        let sut = DefaultDataTransferService(with: NetworkServiceMock(data: dataMock))
        /// When
        sut.request(endpoint: Endpoint(path: "")) { (result: Result<UserMock, Error>) in
            switch result {
            case .success(let user):
                XCTAssertEqual(user.age, 18, "The age doesn't match")
                XCTAssertEqual(user.username, "test", "The username doesn't match")
                XCTAssertEqual(user.email, "test@gmail.com", "The email doesn't match")
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Should not return \(error.localizedDescription)")
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_whenRequestDecodedObjectAtKeyPath_shouldReturnProperObject() {
        /// Given
        let expectation = self.expectation(description: "Should return proper object")
        let jsonMock = ["user": ["age": 18, "email": "test@gmail.com", "username": "test"]] as Any
        let dataMock = try? JSONSerialization.data(withJSONObject: jsonMock, options: .prettyPrinted)
        let sut = DefaultDataTransferService(with: NetworkServiceMock(data: dataMock))
        /// When
        sut.request(endpoint: Endpoint(path: "", keyPath: "user")) { (result: Result<UserMock, Error>) in
            switch result {
            case .success(let user):
                XCTAssertEqual(user.age, 18, "The age doesn't match")
                XCTAssertEqual(user.username, "test", "The username doesn't match")
                XCTAssertEqual(user.email, "test@gmail.com", "The email doesn't match")
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Should not return \(error.localizedDescription)")
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_whenRequestDecodedObjectAtDeepedKeyPath_shouldReturnProperObject() {
        /// Given
        let expectation = self.expectation(description: "Should return proper object")
        let jsonMock = ["data": ["user": ["info": ["age": 18, "email": "test@gmail.com", "username": "test"]]]] as Any
        let dataMock = try? JSONSerialization.data(withJSONObject: jsonMock, options: .prettyPrinted)
        let sut = DefaultDataTransferService(with: NetworkServiceMock(data: dataMock))
        /// When
        sut.request(endpoint: Endpoint(path: "", keyPath: "data/user/info")) { (result: Result<UserMock, Error>) in
            switch result {
            case .success(let user):
                XCTAssertEqual(user.age, 18, "The age doesn't match")
                XCTAssertEqual(user.username, "test", "The username doesn't match")
                XCTAssertEqual(user.email, "test@gmail.com", "The email doesn't match")
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Should not return \(error)")
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    // MARK: Testing Request Generics Conform BaseMappable Array
    func test_whenRequestFailedDecodeArray_shouldReturnParsingError() {
        /// Given
        let expectation = self.expectation(description: "Should return parsing error")
        let sut = DefaultDataTransferService(with: NetworkServiceMock(data: Data()))
        /// When
        sut.request(endpoint: Endpoint(path: "")) { (result: Result<[UserMock], Error>) in
            switch result {
            case .success:
                XCTFail("Should not happen")
            case .failure(let error):
                if case DataTransferError.parsing = error {
                    expectation.fulfill()
                } else {
                    XCTFail("Should not return error: \(error.localizedDescription)")
                }
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_whenRequestDecodedArray_shouldReturnProperArray() {
        /// Given
        let expectation = self.expectation(description: "Should return proper array")
        let jsonMock = [["age": 18, "email": "test@gmail.com", "username": "test"]] as Any
        let dataMock = try? JSONSerialization.data(withJSONObject: jsonMock, options: .prettyPrinted)
        let sut = DefaultDataTransferService(with: NetworkServiceMock(data: dataMock))
        /// When
        sut.request(endpoint: Endpoint(path: "")) { (result: Result<[UserMock], Error>) in
            switch result {
            case .success(let users):
                XCTAssertEqual(users.first?.age, 18, "The age doesn't match")
                XCTAssertEqual(users.first?.username, "test", "The username doesn't match")
                XCTAssertEqual(users.first?.email, "test@gmail.com", "The email doesn't match")
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Should not return \(error.localizedDescription)")
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_whenRequestDecodedArrayAtKeyPath_shouldReturnProperObject() {
        /// Given
        let expectation = self.expectation(description: "Should return proper array")
        let jsonMock = ["user": [["age": 18, "email": "test@gmail.com", "username": "test"]]] as Any
        let dataMock = try? JSONSerialization.data(withJSONObject: jsonMock, options: .prettyPrinted)
        let sut = DefaultDataTransferService(with: NetworkServiceMock(data: dataMock))
        /// When
        sut.request(endpoint: Endpoint(path: "", keyPath: "user")) { (result: Result<[UserMock], Error>) in
            switch result {
            case .success(let users):
                XCTAssertEqual(users.first?.age, 18, "The age doesn't match")
                XCTAssertEqual(users.first?.username, "test", "The username doesn't match")
                XCTAssertEqual(users.first?.email, "test@gmail.com", "The email doesn't match")
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Should not return \(error.localizedDescription)")
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_whenRequestDecodedArrayAtDeepedKeyPath_shouldReturnProperArray() {
        /// Given
        let expectation = self.expectation(description: "Should return proper array")
        let jsonMock = ["data": ["user": ["info": [["age": 18, "email": "test@gmail.com", "username": "test"]]]]] as Any
        let dataMock = try? JSONSerialization.data(withJSONObject: jsonMock, options: .prettyPrinted)
        let sut = DefaultDataTransferService(with: NetworkServiceMock(data: dataMock))
        /// When
        sut.request(endpoint: Endpoint(path: "", keyPath: "data/user/info")) { (result: Result<[UserMock], Error>) in
            switch result {
            case .success(let users):
                XCTAssertEqual(users.first?.age, 18, "The age doesn't match")
                XCTAssertEqual(users.first?.username, "test", "The username doesn't match")
                XCTAssertEqual(users.first?.email, "test@gmail.com", "The email doesn't match")
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Should not return \(error)")
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    // MARK: Testing Mapple Object
    func test_whenRequestDecodedMutableObject_shouldReturnPropperObject() {
        /// Given
        let expectation = self.expectation(description: "Should return proper object")
        let jsonMock = ["name": "David"] as Any
        let dataMock = try? JSONSerialization.data(withJSONObject: jsonMock, options: .prettyPrinted)
        let sut = DefaultDataTransferService(with: NetworkServiceMock(data: dataMock))
        /// When
        sut.request(endpoint: Endpoint(path: "")) { (result: Result<MutableMock, Error>) in
            switch result {
            case .success(let object):
                XCTAssertEqual(object.name, "David", "The name doesn't match")
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Should not return \(error)")
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_whenRequestDecodedMutableArray_shouldReturnPropperArray() {
        /// Given
        let expectation = self.expectation(description: "Should return proper array")
        let jsonMock = [["name": "David"]] as Any
        let dataMock = try? JSONSerialization.data(withJSONObject: jsonMock, options: .prettyPrinted)
        let sut = DefaultDataTransferService(with: NetworkServiceMock(data: dataMock))
        /// When
        sut.request(endpoint: Endpoint(path: "")) { (result: Result<[MutableMock], Error>) in
            switch result {
            case .success(let objects):
                XCTAssertEqual(objects.first?.name, "David", "The name doesn't match")
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Should not return \(error)")
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
}
