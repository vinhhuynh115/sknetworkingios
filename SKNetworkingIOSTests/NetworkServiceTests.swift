//
//  NetworkServiceTests.swift
//  FPTPlayNetworkingTests
//
//  Created by Vinh Huynh on 2/21/20.
//  Copyright © 2020 FPT Play. All rights reserved.
//

import XCTest
@testable import SKNetworkingIOS

class NetworkServiceTests: XCTestCase {
    func test_whenRequestWithEnpointMakingRequestFailed_shouldReturnURLGenerationError() {
        /// Given
        let expectation = self.expectation(description: "Should return \(NetworkServiceError.urlGeneration)")
        let sut = DefaultNetworkService(config: NetworkConfigurableMock(),
                                        networkSession: NetworkSessionManagerMock())
        /// When
        sut.request(endpoint: EndpointMock(path: "&^%&*(", isFullPath: true)) { result in
            switch result {
            case .success:
                XCTFail("Should not happen")
            case .failure(let error):
                if case NetworkServiceError.urlGeneration = error {
                    expectation.fulfill()
                }
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_whenRequestReceivedErrorNotConnectedToInternet_shouldReturnProperError() {
        /// Given
        let expectation = self.expectation(description: "Should return \(NetworkServiceError.notConnected)")
        let errorMock = NSError(domain: "Testing Not Connected", code: URLError.Code.notConnectedToInternet.rawValue, userInfo: nil)
        let sut = DefaultNetworkService(config: NetworkConfigurableMock(),
                                        networkSession: NetworkSessionManagerMock(error: errorMock))
        /// When
        sut.request(endpoint: EndpointMock(path: "testpath")) { result in
            switch result {
            case .success:
                XCTFail("Should not happen")
            case .failure(let error):
                if case NetworkServiceError.notConnected = error {
                    expectation.fulfill()
                }
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_whenRequestReceivedErrorCancelled_shouldReturnProperError() {
        /// Given
        let expectation = self.expectation(description: "Should return \(NetworkServiceError.cancelled)")
        let errorMock = NSError(domain: "Testing Cancelled", code: URLError.Code.cancelled.rawValue, userInfo: nil)
        let sut = DefaultNetworkService(config: NetworkConfigurableMock(),
                                        networkSession: NetworkSessionManagerMock(error: errorMock))
        /// When
        sut.request(endpoint: EndpointMock(path: "testpath")) { result in
            switch result {
            case .success:
                XCTFail("Should not happen")
            case .failure(let error):
                if case NetworkServiceError.cancelled = error {
                    expectation.fulfill()
                }
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    func test_whenRequestReceivedErrorGeneric_shouldReturnProperError() {
        /// Given
        let errorMock = NSError(domain: "Testing Generic Error", code: -1717, userInfo: nil)
        let expectedMessage = "Should return \(NetworkServiceError.generic(errorMock))"
        let expectation = self.expectation(description: expectedMessage)
        let sut = DefaultNetworkService(config: NetworkConfigurableMock(),
                                        networkSession: NetworkSessionManagerMock(error: errorMock))
        /// When
        sut.request(endpoint: EndpointMock(path: "testpath")) { result in
            switch result {
            case .success:
                XCTFail("Should not happen")
            case .failure(let error):
                if case NetworkServiceError.generic(let genericError) = error {
                    XCTAssertEqual((genericError as NSError).code, (errorMock as NSError).code, expectedMessage)
                    expectation.fulfill()
                }
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    func test_whenRequestReceivedErrorAndResponseIsHTTPURLResponse_shouldReturnProperError() {
        /// Given
        let expectedData = "Test_Data".data(using: .utf8) ?? Data()
        let expectedStatusCode = 500
        let expectation = self.expectation(description: "Should return status code \(expectedStatusCode)")
        let networkSessionManagerMock = NetworkSessionManagerMock(
            data: expectedData,
            error: NSError(domain: "Testing", code: -1, userInfo: nil),
            response: HTTPURLResponse(url: URL(string: "test_url")!, statusCode: expectedStatusCode, httpVersion: "1.1", headerFields: [:])
        )
        let sut = DefaultNetworkService(config: NetworkConfigurableMock(),
                                        networkSession: networkSessionManagerMock)
        /// When
        sut.request(endpoint: EndpointMock(path: "testpath")) { result in
            switch result {
            case .success:
                XCTFail("Should not happen")
            case .failure(let error):
                if case NetworkServiceError.error(let statusCode, let data) = error {
                    XCTAssertEqual(data, expectedData, "Should return the expected data")
                    XCTAssertEqual(statusCode, expectedStatusCode, "Should return status code \(expectedStatusCode)")
                    expectation.fulfill()
                }
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_whenRequestSuccessedWithEmptyData_shouldReturnNoResponseDataError() {
        /// Given
        let expectation = self.expectation(description: "Should return \(NetworkServiceError.noResponseData)")
        let sut = DefaultNetworkService(config: NetworkConfigurableMock(),
                                        networkSession: NetworkSessionManagerMock())
        /// When
        sut.request(endpoint: EndpointMock(path: "testpath")) { result in
            switch result {
            case .success:
                XCTFail("Should not happen")
            case .failure(let error):
                if case NetworkServiceError.noResponseData = error {
                    expectation.fulfill()
                }
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
    
    func test_whenRequestSuccessed_shouldReturnProperData() {
        /// Given
        let expectedData = "Test_Data".data(using: .utf8) ?? Data()
        let expectation = self.expectation(description: "Should return \(NetworkServiceError.noResponseData)")
        let sut = DefaultNetworkService(config: NetworkConfigurableMock(),
                                        networkSession: NetworkSessionManagerMock(data: expectedData))
        /// When
        sut.request(endpoint: EndpointMock(path: "testpath")) { result in
            switch result {
            case .success(let data):
                XCTAssertEqual(data, expectedData, "Should return the expected data")
                expectation.fulfill()
            case .failure:
                XCTFail("Should not happen")
            }
        }
        /// Then
        wait(for: [expectation], timeout: 0.1)
    }
}
